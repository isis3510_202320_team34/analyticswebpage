from datetime import datetime, timedelta
import json
import firebase_admin
from firebase_admin import credentials, db
from firebase_admin import firestore
from google.cloud.firestore import SERVER_TIMESTAMP
from datetime import datetime
import pandas as pd
import os

def refresh_data():

    credential_file_name = 'unifood-10fed-firebase-adminsdk-wa6ap-ab1089b142.json'
    # Initialize Firebase Admin SDK
    cred = credentials.Certificate(f"dashboard\static\credentials\{credential_file_name}" )
    # Check if the default app has already been initialized
    if not firebase_admin._apps:
        firebase_admin.initialize_app(cred)
    else:
        # If already initialized, you can use the existing app
        default_app = firebase_admin.get_app()

    db = firestore.client()
    path_back_up = save_backup(db)

    with open(path_back_up, "r") as file:
        data = json.load(file)

    print(data.keys())

    update_flatten_data(data)

# Custom JSON encoder
class FirestoreEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        elif obj == SERVER_TIMESTAMP:
            return "SERVER_TIMESTAMP"
        return json.JSONEncoder.default(self, obj)



# Function to recursively fetch all documents from a collection
def fetch_collection(collection_ref):
    docs = collection_ref.stream()
    data = {}
    for doc in docs:
        data[doc.id] = doc.to_dict()
    return data

# Fetch all data from Firestore
def backup_firestore(db):
    all_collections = db.collections()
    backup_data = {}
    for collection in all_collections:
        backup_data[collection.id] = fetch_collection(collection)
    return backup_data

# Backup the data and save it as 'backup.json'
def save_backup(db):
    data = backup_firestore(db)
    backup_file_name = 'backup.json'
    back_up_path = f"dashboard/static/{backup_file_name}"

    with open(back_up_path, 'w') as backup_file:
        json.dump(data, backup_file, cls=FirestoreEncoder, indent=4)
    return back_up_path


def update_flatten_data(data):
    orders_data = data['orders']
    reviews_data = data['reviews']
    reservations_data = data['reservations']
    restaurants_schedules_data = data['restaurantSchedules']

    restaurants_data = data['restaurants']
    users_data = data['users']
    #------------------------- ORDERS DATA -------------------------#

    # Convert orders data to DataFrame
    orders_df = pd.DataFrame(orders_data.values())
    #transform orderDate-Hour to date-hour formate 
    orders_df['orderDate-Hour'] = pd.to_datetime(orders_df['orderDate-Hour'], format='%m/%d/%Y-%H:%M')
    # Flatten dishesId column  
    orders_df = orders_df.explode('dishesId')

    #------------------------- REVIEWS DATA -------------------------#
    # Convert reviews data to DataFrame
    reviews_df = pd.DataFrame(reviews_data.values())
    #transform reviewDate to date formate
    reviews_df['createdAt'] = pd.to_datetime(reviews_df['createdAt']).dt.floor('S')
    reviews_df['updatedAt'] = pd.to_datetime(reviews_df['updatedAt']).dt.floor('S')

    #------------------------- RESERVATIONS DATA -------------------------#
    # Convert reservations data to DataFrame
    reservations_df = pd.DataFrame(reservations_data.values())
    #transform reservationDate to date formate
    reservations_df['reservationDate'] = pd.to_datetime(reservations_df['reservationDate']).dt.floor('S')

    #------------------------- RESTAURANTS SCHEDULES DATA -------------------------#
    # Convert restaurants schedules data to DataFrame
    restaurants_schedules_df = pd.DataFrame(restaurants_schedules_data.values())
    #transform scheduleDate to date formate
    restaurants_schedules_df['openTime'] = pd.to_datetime(restaurants_schedules_df['openTime']).dt.strftime('%H:%M')
    restaurants_schedules_df['closeTime'] = pd.to_datetime(restaurants_schedules_df['closeTime']).dt.strftime('%H:%M')

    ########## RESTAURANTS DATA -------------------------#
    # Flatten restaurants data
    restaurants_list = []
    for restaurant_key, restaurant in restaurants_data.items():
        for dish in restaurant['menu']:
            #Change the format of the dish, by adding dish_ at the beginning of the key
            dish = {f"dish_{key}": value for key, value in dish.items()}
            flat_restaurant = restaurant.copy()
            flat_restaurant.pop('menu', None)  # Remove the menu key
            flat_restaurant.update(dish)  # Update with dish attributes
            flat_restaurant['restaurantId'] = restaurant_key  # Add the restaurant ID
            restaurants_list.append(flat_restaurant)

    restaurants_df = pd.DataFrame(restaurants_list)

    #------------------------- USERS DATA -------------------------#
    # Convert users data to DataFrame
    users_df = pd.DataFrame(users_data.values())

    ##------------------------- SAVE TO CSV FILES -------------------------#
    # Save the dataframes to CSV
    path_to_save = "dashboard/flattened_data/"
    orders_df.to_csv(path_to_save + "orders_flattened.csv" , index=False)
    reviews_df.to_csv(path_to_save + "reviews_flattened.csv", index=False)
    restaurants_df.to_csv(path_to_save + "restaurants_flattened.csv", index=False)
    users_df.to_csv(path_to_save + "users_flattened.csv", index=False)
    reservations_df.to_csv(path_to_save + "reservations_flattened.csv", index=False)
    restaurants_schedules_df.to_csv(path_to_save + "restaurants_schedules_flattened.csv", index=False)