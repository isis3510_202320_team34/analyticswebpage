from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('dashboard_page_6', views.dashboard_page_6, name='dashboard_page_6'),
    path('dashboard_page_1', views.dashboard_page_1, name='dashboard_page_1'),
    path('dashboard_page_2', views.dashboard_page_2, name='dashboard_page_2'),
    path('dashboard_page_3', views.dashboard_page_3, name='dashboard_page_3'),
    path('dashboard_page_4', views.dashboard_page_4, name='dashboard_page_4'),
    path('dashboard_page_5', views.dashboard_page_5, name='dashboard_page_5'),
    
    path('ajax/refresh-data/', views.ajax_refresh_data, name='ajax_refresh_data'),
]