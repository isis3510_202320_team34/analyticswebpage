from django.shortcuts import render
import matplotlib.pyplot as plt
import pandas as pd
from io import BytesIO
import base64
from django.http import JsonResponse

from dashboard.views_dashboards.view_dashboard_page_1 import view_dashboard_page_1
from dashboard.views_dashboards.view_dashboard_page_2 import view_dashboard_page_2
from dashboard.views_dashboards.view_dashboard_page_3 import view_dashboard_page_3
from dashboard.views_dashboards.view_dashboard_page_4 import view_dashboard_page_4
from dashboard.views_dashboards.view_dashboard_page_6 import view_dashboard_page_6
from .analytics_functions.refresh_data import refresh_data


def ajax_refresh_data(request):
    # Call your refresh_data function
    refresh_data()
    # Return a JSON response
    return JsonResponse({"success": True})

def index(request):

    return render(request, "index.html")

def dashboard_page_1(request):
    return view_dashboard_page_1(request)

def dashboard_page_2(request):
    return view_dashboard_page_2(request)

def dashboard_page_3(request):
    return view_dashboard_page_3(request)

def dashboard_page_4(request):
    return view_dashboard_page_4(request)

def dashboard_page_5(request):
    return view_dashboard_page_5(request)

def dashboard_page_6(request):
    return view_dashboard_page_6(request)

