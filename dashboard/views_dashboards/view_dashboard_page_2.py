from django.shortcuts import render
import matplotlib.pyplot as plt
import pandas as pd
from io import BytesIO
import base64

def view_dashboard_page_2(request):
      # Get the data from the dat
     # Get the restaurant id from the request
        # Load the data from the CSV files
    initial_restaurant_id = 'A8Ojz3VlKdmb0nSgSx3h'
    orders_path = 'dashboard/flattened_data/orders_flattened.csv'
    restaurants_path = 'dashboard/flattened_data/restaurants_flattened.csv'
    orders_df = pd.read_csv(orders_path)
    restaurants_df = pd.read_csv(restaurants_path)

    # Get the restaurant id from the request or use a default
    restaurant_id = request.GET.get('restaurant_id', initial_restaurant_id)

    #Find the real name of the restaurant
    restaurant_name = restaurants_df[restaurants_df['restaurantId'] == restaurant_id]['name'].values[0]


    # Generate the top most time expensive dishes plot
    fig, axs = plt.subplots(3, 1, figsize=(10, 18))  # 3 rows, 1 column for subplots

    top_time_expensive_dishes = calculate_top_time_expensive_dishes(restaurant_id, orders_df)
    
    top_time_expensive_dishes.plot(kind='bar', ax=axs[0])
    axs[0].set_title(f'Top 5 Most Time Expensive Dishes for Restaurant: {restaurant_name}')
    axs[0].set_xlabel('Dish Name')
    axs[0].set_ylabel('Average preparation time (Minutes)')
    axs[0].tick_params(axis='x', rotation=45)

    # Generate the top Most time expensive restaurants based on their avergae dish prep time plot
    top_time_expensive_restaurants = calculate_top_time_expensive_restaurants( orders_df, restaurants_df)

    top_time_expensive_restaurants.plot(kind='bar', ax=axs[1])
    axs[1].set_title(f'Top 5 Most Time Expensive Restaurants Based on their average dish prep time')
    axs[1].set_xlabel('Dish Name')
    axs[1].set_ylabel('Average preparation time for all the dishes(Minutes)')
    axs[1].tick_params(axis='x', rotation=45)

    # Generate the last month's top 5 and bottom 5 restaurants plots
    
    top_waiting_time_restaurants = calculate_top_waiting_restaurants(restaurants_df)
    
    top_waiting_time_restaurants.plot(kind='bar', ax=axs[2])
    axs[2].set_title('Top 5 Restaurants with highest waiting times')
    axs[2].set_xlabel('Restaurant Name')
    axs[2].set_ylabel('Waiting time (Minutes)')
    axs[2].tick_params(axis='x', rotation=45)

    # Adjust layout to ensure everything fits without overlapping
    plt.tight_layout()

    # Save it to a temporary buffer.
    buf = BytesIO()
    plt.savefig(buf, format='png')

    # Embed the result in the html output.
    data = base64.b64encode(buf.getbuffer()).decode("ascii")

    restaurants = restaurants_df[['restaurantId', 'name']].to_dict('records')
    # Select uniques restaurants
    restaurants = list({v['restaurantId']:v for v in restaurants}.values())
    # Sort the restaurants by name
    restaurants = sorted(restaurants, key=lambda k: k['name'])
    #print(restaurants)

    # Render the template with the graph data and the list of restaurants
    return render(request, "dashboard_page_2.html", {
        "graph": data,
        "restaurants": restaurants,
        "selected_restaurant_id": restaurant_id
    })




# A function to calculate the top 5 most time expensive dishes for a given restaurant
def calculate_top_time_expensive_dishes(restaurant_id, orders_df):
    filtered_orders = orders_df[orders_df['restaurantId'] == restaurant_id]
    # Group by dish ID and calculate the total duration for each dish
    dish_duration = filtered_orders.groupby('description')['orderDurationHours'].mean()

    # Find the top 5 most time-consuming dishes
    top_time_expensive_dishes = dish_duration.nlargest(5)

    return top_time_expensive_dishes

#A function to calculate the top 5 most time expensive restaurants given the average time it takes to prepare all their dishes
def calculate_top_time_expensive_restaurants(orders_df, restaurants_df):
    # Group orders by restaurant and dish ID to calculate the total duration for each dish in each restaurant
    restaurant_dish_duration = orders_df.groupby(['restaurantId', 'dishesId'])['orderDurationHours'].sum().reset_index()

    # Calculate the average duration for each restaurant
    avg_restaurant_duration = restaurant_dish_duration.groupby('restaurantId')['orderDurationHours'].mean()

    # Find the top 5 most time-expensive restaurants based on the average duration
    top_5_restaurants_with_avg_duration = avg_restaurant_duration.nlargest(5)

     # Create a dictionary to map restaurant IDs to their names
    restaurant_names = restaurants_df.set_index('restaurantId')['name'].to_dict()

    # Replace the restaurant IDs with names in the top and bottom 5 counts
    top_5_restaurants_with_avg_duration = top_5_restaurants_with_avg_duration.rename(index=restaurant_names)

    return top_5_restaurants_with_avg_duration




def calculate_top_waiting_restaurants(restaurants_df):
    # Convert 'waitingTime' column to numeric
    restaurants_df['waitingTime'] = pd.to_numeric(restaurants_df['waitingTime'], errors='coerce')

    # Fill missing values with 10
    restaurants_df['waitingTime'].fillna(10, inplace=True)

    # Group by restaurant and calculate the average waiting time
    avg_waiting_time_per_restaurant = restaurants_df.groupby('restaurantId')['waitingTime'].mean()

    # Find the top 5 restaurants with longer waiting times
    top_5_waiting_restaurants = avg_waiting_time_per_restaurant.nlargest(5)

    restaurant_names = restaurants_df.set_index('restaurantId')['name'].to_dict()

    # Replace the restaurant IDs with names in the top and bottom 5 counts
    top_5_waiting_restaurants = top_5_waiting_restaurants.rename(index=restaurant_names)


    return top_5_waiting_restaurants
