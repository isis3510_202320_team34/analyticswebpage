from django.shortcuts import render
import matplotlib.pyplot as plt
import pandas as pd
from io import BytesIO
import base64

def view_dashboard_page_1(request):
    # Get the data from the dat
     # Get the restaurant id from the request
        # Load the data from the CSV files
    initial_restaurant_id = 'A8Ojz3VlKdmb0nSgSx3h'
    orders_path = 'dashboard/flattened_data/orders_flattened.csv'
    restaurants_path = 'dashboard/flattened_data/restaurants_flattened.csv'
    orders_df = pd.read_csv(orders_path)
    restaurants_df = pd.read_csv(restaurants_path)

    # Get the restaurant id from the request or use a default
    restaurant_id = request.GET.get('restaurant_id', initial_restaurant_id)
    #print(f'restaurant_id {restaurant_id}'.center(50, '-'))

    #Find the real name of the restaurant
    restaurant_name = restaurants_df[restaurants_df['restaurantId'] == restaurant_id]['name'].values[0]


    # Generate the top dishes plot
    top_dishes = calculate_top_dishes(restaurant_id, orders_df)
    fig, axs = plt.subplots(3, 1, figsize=(10, 18))  # 3 rows, 1 column for subplots

    top_dishes.plot(kind='bar', ax=axs[0])
    axs[0].set_title(f'Top 5 Most Sold Dishes for Restaurant ID: {restaurant_name}')
    axs[0].set_xlabel('Dish Name')
    axs[0].set_ylabel('Number of Orders')
    axs[0].tick_params(axis='x', rotation=45)

    # Generate the last month's top 5 and bottom 5 restaurants plots
    last_month_orders, top_5_restaurants_names, bottom_5_restaurants_names = calculate_restaurants_orders(orders_df, restaurants_df)
    
    top_5_restaurants_names.plot(kind='bar', ax=axs[1], color='green', edgecolor='black')
    axs[1].set_title('Top 5 Restaurants with Most Orders Last Month')
    axs[1].set_xlabel('Restaurant Name')
    axs[1].set_ylabel('Number of Orders')
    axs[1].tick_params(axis='x', rotation=45)

    bottom_5_restaurants_names.plot(kind='bar', ax=axs[2], color='red', edgecolor='black')
    axs[2].set_title('Bottom 5 Restaurants with Least Orders Last Month')
    axs[2].set_xlabel('Restaurant Name')
    axs[2].set_ylabel('Number of Orders')
    axs[2].tick_params(axis='x', rotation=45)

    # Adjust layout to ensure everything fits without overlapping
    plt.tight_layout()

    # Save it to a temporary buffer.
    buf = BytesIO()
    plt.savefig(buf, format='png')
    # Embed the result in the html output.
    data = base64.b64encode(buf.getbuffer()).decode("ascii")

    restaurants = restaurants_df[['restaurantId', 'name']].to_dict('records')
    # Select uniques restaurants
    restaurants = list({v['restaurantId']:v for v in restaurants}.values())
    # Sort the restaurants by name
    restaurants = sorted(restaurants, key=lambda k: k['name'])
    #print(restaurants)

    # Render the template with the graph data and the list of restaurants
    return render(request, "dashboard_page_1.html", {
        "graph": data,
        "restaurants": restaurants,
        "selected_restaurant_id": restaurant_id
    })




# A function to calculate the top 5 most sold dishes for a given restaurant
def calculate_top_dishes(restaurant_id, orders_df):
    filtered_orders = orders_df[orders_df['restaurantId'] == restaurant_id]
    # Count the occurrences of each dish name and sort them
    top_dishes = filtered_orders['description'].value_counts().nlargest(5)
    return top_dishes

# A function to calculate the last month's top 5 and bottom 5 restaurants
def calculate_restaurants_orders(orders_df, restaurants_df):
    # Convert the 'orderDate-Hour' column to datetime
    orders_df['orderDate-Hour'] = pd.to_datetime(orders_df['orderDate-Hour'])

    # Find the last date in the dataset to determine the last month
    last_order_date = orders_df['orderDate-Hour'].max()
    last_month = last_order_date.month

    # Filter the orders to include only those from the last month
    last_month_orders = orders_df[orders_df['orderDate-Hour'].dt.month == last_month]

    # Count the number of orders for each restaurant
    restaurant_order_counts = last_month_orders['restaurantId'].value_counts()

    # Identify the top 5 and bottom 5 restaurants based on order counts
    top_5_restaurants = restaurant_order_counts.head(5)
    bottom_5_restaurants = restaurant_order_counts.tail(5)

    # Create a dictionary to map restaurant IDs to their names
    restaurant_names = restaurants_df.set_index('restaurantId')['name'].to_dict()

    # Replace the restaurant IDs with names in the top and bottom 5 counts
    top_5_restaurants_names = top_5_restaurants.rename(index=restaurant_names)
    bottom_5_restaurants_names = bottom_5_restaurants.rename(index=restaurant_names)

    return last_month_orders, top_5_restaurants_names, bottom_5_restaurants