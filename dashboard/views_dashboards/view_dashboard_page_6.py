from django.shortcuts import render
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt
import pandas as pd
from io import BytesIO
import base64


def view_dashboard_page_6(request):

    df = pd.read_csv('dashboard/flattened_data/reviews_flattened.csv')
    df_restaurants = pd.read_csv('dashboard/flattened_data/restaurants_flattened.csv')

    df['createdAt'] = pd.to_datetime(df['createdAt'])

    df['month'] = df['createdAt'].dt.to_period('M')

    result = df.groupby(['month', 'restaurantId']).agg({'rate': 'mean'}).reset_index()

    max_rate_per_month = result.loc[result.groupby('month')['rate'].idxmax()]

    result = pd.merge(max_rate_per_month, df_restaurants[['restaurantId', 'name']], on='restaurantId', how='left')

    print(result[['month', 'name', 'rate']])
    
    plt.switch_backend('Agg')
    # Generar la gráfica
    plt.figure(figsize=(12, 6))
    for month, group in result.groupby('month'):
        plt.bar(str(month), group['rate'].values, label=group['name'].values)

    plt.xlabel('Month')
    plt.ylabel('Average rate')
    plt.title('Top Rated Restaurants per month')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    plt.xticks(rotation=45)
    plt.tight_layout()

    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    buffer.close()

    context = {'chart_base64': base64.b64encode(image_png).decode('utf-8')}

    return render(request, 'dashboard_page_6.html', context)


    
def restaurantsTopRated(reviews_df, restaurants_df):
    reviews_df['month'] = reviews_df['createdAt'].dt.to_period('M')

    # Encuentra el índice del restaurante con el mayor rate por cada mes
    idx = reviews_df.groupby(['month', 'restaurantId'])['rate'].idxmax()

    # Filtra el DataFrame original usando los índices encontrados
    result = reviews_df.loc[idx, ['month', 'restaurantId', 'rate']]

    return result[['month', 'name', 'rate']]

