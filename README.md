## Prerequisites

Before you begin, ensure you have the following installed on your system:
- Python 3.x
- pip (Python package manager)

## Setting Up the Environment

Follow these steps to set up the virtual environment and activate it:

For Windows:
```bash
python3 -m venv ./mobilesEnv
.\mobilesEnv\Scripts\activate
```

For macOS and Linux:
```bash
python3 -m venv ./mobilesEnv
source mobilesEnv/bin/activate
```

## Installing the Application

Navigate to the application directory:
```bash
cd ./analyticswebpage
```

Install Django and other required dependencies:
```bash
pip3 install django
pip3 install matplotlib
pip3 install pandas
pip3 install firebase_admin
pip3 install google-cloud-firestore
```

## Database Setup

Set up the initial database migrations with the following commands:
```bash
python3 manage.py makemigrations
python3 manage.py migrate
```

## Running the Server

Once the setup is complete, you can start the server with:
```bash
python3 manage.py runserver
```

The server should now be running on `http://127.0.0.1:8000/`.

Open your web browser and navigate to the provided local server URL to view the application.
